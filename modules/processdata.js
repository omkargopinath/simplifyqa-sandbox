const DBManager = require("./dbmanager");
const dbManager = new DBManager();
const uuid = require('uuid');
const crypto = require('crypto');
const oldCustomerIdGlobal = 2;

const ProcessData = function () {
}

let processDAta = new ProcessData()

ProcessData.prototype.createCustomer = async function (objValue) {
    dbManager.getData("user", { username: new RegExp("^" + objValue.email + "$", "i"), deleted: false }, { email: 1 }, function (err, data) {
        if (err || data.length) {
            console.log("not possible")
            return;
        } else {
            crypto.pbkdf2(objValue.password, 'salt', 100000, 50, 'sha512', (err, key) => {
                if (err) {
                    console.log(err);
                    return
                } else {
                    objValue.password = key.toString('hex');
                    adduser(objValue, (err, data1) => {
                        if (!err) {
                            processDAta.copyCustomerData(objValue.oldCustomerId, data1.customerId, data1.id)
                            console.log("customer created")
                        } else {
                            console.log(err)
                        }

                    })
                }

            })
        }
    })
}


// ProcessData.prototype.startCopy = async function () {
//     let actions = await dbManager.find("action", { customerId: 1, deleted: false })
//     console.log(actions)
// }

// ProcessData.prototype.looping = async function () {
//     let count = await dbManager.count("objecttemplate")
//     console.log(count)
//     let loopCount = Math.ceil(count / 10)
//     let skip = 0;
//     let limit = 10;
//     for (let i = 0; i < loopCount; i++) {
//         let objecttemplate = await dbManager.findWithSkipLimit("objecttemplate", {}, skip, limit)
//         console.log(objecttemplate)
//         skip = skip + 10

//     }
// }

// ProcessData.prototype.new = async function () {
//     let newOne = await dbManager.create("omkar46")
//     console.log(newOne)
// }

// ProcessData.prototype.insertNewData = async function () {
//     let newData = await dbManager.insertData("omkar46", { name: "omkar" })
//     console.log(newData)
// }

// ProcessData.prototype.startCopy1 = async function () {
//     let objecttemplate = await dbManager.count("objecttemplate")
//     console.log(objecttemplate)
// }

// ProcessData.prototype.fetchAndAdd = async function () {
//     let idMap = {}
//     let newActions = await dbManager.find("action", {})
//     for (let i = 0; i < newActions.length; i++) {
//         let data = dbManager.getNextSequence("omkarNew")
//         try {
//             if (data.seq) {
//                 idMap[newActions[i].id] = data.seq
//                 newActions[i].id = data.seq
//                 // console.log(idMap)
//             }
//         } catch (err) {
//             console.log(err)
//         }
//     }
//     let newDataToInsert = await dbManager.insertData("omkarNew", newActions)
//     // console.log(newDataToInsert)

// }


ProcessData.prototype.copyCustomerData = async function (oldCustomerId1, customerId1, userId) {
    let oldCustomerId = oldCustomerId1;
    let customerId = customerId1;
    let uniqueKey = uuid.v4();
    processDAta.fetchandsaveaction(oldCustomerId, customerId, uniqueKey, userId);
}

ProcessData.prototype.fetchandsaveaction = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let actionMap = {}
    let newActionData = await dbManager.find("action", { customerId: oldCustomerId, deleted: false });
    for (let i = 0; i < newActionData.length; i++) {
        delete newActionData[i]._id;
        newActionData[i].createdBy = userId;
        newActionData[i].updatedBy = userId;
        newActionData[i].isDefault = true;
        newActionData[i].createdOn = new Date()
        newActionData[i].updatedOn = new Date()
        newActionData[i].customerId = customerId;
        let data = await dbManager.getNextSequence("action")
        try {
            if (data.seq) {
                actionMap[newActionData[i].id] = data.seq
                newActionData[i].id = data.seq
            }
        }
        catch (err) {
            console.log(err)
        }

    }
    //save map in document
    await dbManager.insertData("customer_reference_data", { id: uniqueKey, actionMap: actionMap })
    await dbManager.insertData("action", newActionData)
    console.log("Action is done")
    processDAta.fetchandobjTemplate(oldCustomerId, customerId, uniqueKey, userId);
}

ProcessData.prototype.fetchandobjTemplate = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let objTemplateMap = {};
    let refData = await dbManager.find("customer_reference_data", { id: uniqueKey });
    let newObjTempData = await dbManager.find("objecttemplate", { customerId: oldCustomerId, deleted: false });
    for (let i = 0; i < newObjTempData.length; i++) {
        delete newObjTempData[i]._id;
        newObjTempData[i].createdBy = userId;
        newObjTempData[i].updatedBy = userId;
        newObjTempData[i].isDefault = true;
        newObjTempData[i].createdOn = new Date()
        newObjTempData[i].updatedOn = new Date()
        newObjTempData[i].customerId = customerId;
        let data = await dbManager.getNextSequence("objecttemplate")
        try {
            if (data.seq) {
                objTemplateMap[newObjTempData[i].id] = data.seq
                newObjTempData[i].id = data.seq;
                for (let j = 0; j < newObjTempData[i].action.length; j++) {
                    newObjTempData[i].action[j].actionId = refData[0]["actionMap"][newObjTempData[i].action[j].actionId];
                }
            }
        } catch (err) {
            console.log(err)
        }

    }
    await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { objTemplateMap: objTemplateMap } })
    await dbManager.insertData("objecttemplate", newObjTempData);
    console.log("objtemp is done")
    processDAta.fetchAndSaveProject(oldCustomerId, customerId, uniqueKey, userId);
}

ProcessData.prototype.fetchAndSaveProject = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let projectMap = {}
    let newProjectData = await dbManager.find("project", { customerId: oldCustomerId, deleted: false });
    for (let i = 0; i < newProjectData.length; i++) {
        delete newProjectData[i]._id
        newProjectData[i].createdBy = userId;
        newProjectData[i].updatedBy = userId;
        newProjectData[i].isDefault = true;
        newProjectData[i].createdOn = new Date()
        newProjectData[i].updatedOn = new Date()
        newProjectData[i].customerId = customerId
        let data = await dbManager.getNextSequence("project")
        try {
            if (data.seq) {
                projectMap[newProjectData[i].id] = data.seq
                newProjectData[i].id = data.seq
                newProjectData[i].code = "PR-" + customerId + data.seq

            }
        } catch (err) {
            console.log(err)
        }
    }
    await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { projectMap: projectMap } })
    await dbManager.insertData("project", newProjectData)
    console.log("project is done")
    processDAta.fetchAndSaveModule(oldCustomerId, customerId, uniqueKey, userId);
}
//release
//iteration

ProcessData.prototype.fetchAndSaveModule = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let moduleMap = {}
    let proj = await dbManager.find("project", { customerId: oldCustomerId, deleted: false })
    let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
    for (let i = 0; i < proj.length; i++) {
        let newModuleData = await dbManager.find("module", { projectId: proj[i].id, deleted: false });
        if (newModuleData.length) {
            for (let j = 0; j < newModuleData.length; j++) {
                delete newModuleData[j]._id
                newModuleData[j].createdBy = userId;
                newModuleData[j].updatedBy = userId;
                newModuleData[j].isDefault = true;
                newModuleData[j].createdOn = new Date()
                newModuleData[j].updatedOn = new Date()
                newModuleData[j].customerId = customerId
                newModuleData[j].projectId = customerRefData[0].projectMap[newModuleData[j].projectId]
                try {
                    let data = await dbManager.getNextSequence("module")
                    if (data.seq) {
                        moduleMap[newModuleData[j].id] = data.seq
                        newModuleData[j].id = data.seq
                        newModuleData[j].code = "MD-" + customerId + data.seq
                    }
                } catch (err) {
                    console.log(err)
                }
            }
            await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { moduleMap: moduleMap } })
            await dbManager.insertData("module", newModuleData)
        }
    }
    console.log("module is done")
    processDAta.fetchAndSaveParameter(oldCustomerId, customerId, uniqueKey, userId);
}

ProcessData.prototype.fetchAndSaveParameter = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let parameterMap = {}
    let proj = await dbManager.find("project", { customerId: oldCustomerId, deleted: false })
    let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
    for (let i = 0; i < proj.length; i++) {
        let newParameterData = await dbManager.find("parameter", { projectId: proj[i].id, deleted: false });
        if (newParameterData.length) {
            for (let j = 0; j < newParameterData.length; j++) {
                delete newParameterData[j]._id
                newParameterData[j].createdBy = userId;
                newParameterData[j].updatedBy = userId;
                newParameterData[j].isDefault = true;
                newParameterData[j].createdOn = new Date()
                newParameterData[j].updatedOn = new Date()
                newParameterData[j].customerId = customerId
                newParameterData[j].projectId = customerRefData[0].projectMap[newParameterData[j].projectId]
                if (newParameterData[j].moduleId) newParameterData[j].moduleId = customerRefData[0].moduleMap[newParameterData[j].moduleId]
                try {
                    let data = await dbManager.getNextSequence("parameter")
                    if (data.seq) {
                        parameterMap[newParameterData[j].id] = data.seq
                        newParameterData[j].id = data.seq
                        newParameterData[j].code = "PM-" + customerId + data.seq
                    }

                } catch (err) {
                    console.log(err)
                }
            }
            await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { parameterMap: parameterMap } })
            await dbManager.insertData("parameter", newParameterData)

        }
    }
    console.log("param is done")
    processDAta.fetchAndSaveObjects(oldCustomerId, customerId, uniqueKey, userId);
}

ProcessData.prototype.fetchAndSaveObjects = async function (oldCustomerId, customerId, uniqueKey, userId) {
    try {
        let objectMap = {}
        let count = await dbManager.count("object", { customerId: oldCustomerId, deleted: false })
        // let loopCount=Math.ceil(count/10)
        let skip = 0
        let limit = 100
        let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
        for (let j = 0; j < count;) {
            let newObjectsData = await dbManager.findWithSkipLimit("object", { customerId: oldCustomerId, deleted: false }, skip, limit)
            for (let i = 0; i < newObjectsData.length; i++) {
                delete newObjectsData[i]._id
                newObjectsData[i].createdBy = userId;
                newObjectsData[i].updatedBy = userId;
                newObjectsData[i].isDefault = true;
                newObjectsData[i].createdOn = new Date()
                newObjectsData[i].updatedOn = new Date()
                newObjectsData[i].customerId = customerId
                newObjectsData[i].projectId = customerRefData[0].projectMap[newObjectsData[i].projectId]
                if (newObjectsData[i].objtemplateId) newObjectsData[i].objtemplateId = customerRefData[0].objTemplateMap[newObjectsData[i].objtemplateId]
                if (newObjectsData[i].moduleId) newObjectsData[i].moduleId = customerRefData[0].moduleMap[newObjectsData[i].moduleId]
                let data = await dbManager.getNextSequence("object")
                if (data.seq) {
                    objectMap[newObjectsData[i].id] = data.seq
                    newObjectsData[i].id = data.seq
                    newObjectsData[i].code = "OB-" + customerId + data.seq
                }
            }
            await dbManager.insertData("object", newObjectsData)
            skip = skip + 100;
            j = j + 100;
        }
        await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { objectMap: objectMap } })
        console.log("success =>", uniqueKey)
    } catch (error) {
        console.log(error)
    }
    console.log("object is done")
    processDAta.fetchFunctions(oldCustomerId, customerId, uniqueKey, userId)
}

ProcessData.prototype.fetchFunctions = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let functionMap = {}
    let funcsData = await dbManager.find("functions", { customerId: oldCustomerId, deleted: false })
    let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
    for (let i = 0; i < funcsData.length; i++) {
        delete funcsData[i]._id;
        funcsData[i].createdBy = userId;
        funcsData[i].updatedBy = userId;
        funcsData[i].isDefault = true;
        funcsData[i].createdOn = new Date()
        funcsData[i].updatedOn = new Date()
        funcsData[i].customerId = customerId;
        funcsData[i].projectId = customerRefData[0].projectMap[funcsData[i].projectId]
        if (funcsData[i].moduleId) funcsData[i].moduleId = customerRefData[0].moduleMap[funcsData[i].moduleId]
        // let data = await dbManager.getCurrentSequence("functions")

        for (let j = 0; j < funcsData[i].versions.length; j++) {
            let ver = funcsData[i].versions[j].version
            let type = 'FN'
            // let parentId = data[0].seq+1
            let oldParentId = funcsData[i].id
            let steps = await processDAta.fetchSteps(oldCustomerId, customerId, uniqueKey, type, ver, oldParentId)
            let fn
            if (!functionMap[funcsData[i].id]) {
                let tempData = Object.assign({}, funcsData[i]);
                delete tempData.id
                fn = await processDAta.handletcfn("functions", { data: tempData, steps: JSON.parse(JSON.stringify(steps)) })
                functionMap[funcsData[i].id] = fn.id;
            } else {
                fn = await processDAta.handletcfn("functions", { data: { copyFlag: false, diff_field: {}, id: functionMap[funcsData[i].id], itsNewVersion: true, tdcopy: false, updateFields: {} }, steps: steps })

            }
        }
        // try {
        //     if (data.seq) {
        //         functionMap[funcsData[i].id] = data.seq
        //         funcsData[i].id = data.seq
        //         funcsData[i].code = "FN-"+customerId+data.seq
        //     }
        // }
        // catch (err) {
        //     console.log(err)
        // }

    }
    await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { functionMap: functionMap } })
    console.log("function is done")
    // await dbManager.insertData("functions", funcsData)
    processDAta.fetchTestcases(oldCustomerId, customerId, uniqueKey, userId)

}

ProcessData.prototype.fetchSteps = async function (oldCustomerId, customerId, uniqueKey, type, ver, oldParentId) {
    // let stepsMap = {}
    return new Promise(async (resolve, reject) => {
        try {
            let newStepsData = await dbManager.find("steps", { customerId: oldCustomerId, deleted: false, type: type, version: ver, parentId: oldParentId })
            let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
            for (let i = 0; i < newStepsData.length; i++) {
                let randomNumber = uuid.v4();
                delete newStepsData[i]._id
                delete newStepsData[i].id
                newStepsData[i].createdOn = new Date()
                newStepsData[i].updatedOn = new Date()
                newStepsData[i].customerId = customerId
                newStepsData[i].projectId = customerRefData[0].projectMap[newStepsData[i].projectId]
                // newStepsData[i].parentId = parentId
                if (newStepsData[i].objectId) newStepsData[i].objectId = customerRefData[0].objectMap[newStepsData[i].objectId]
                if (newStepsData[i].moduleId) newStepsData[i].moduleId = customerRefData[0].moduleMap[newStepsData[i].moduleId]
                if (newStepsData[i].actionId) newStepsData[i].actionId = customerRefData[0].actionMap[newStepsData[i].actionId]
                // stepsMap[newStepsData[i].seq] = randomNumber
                // newStepsData[i].seq = randomNumber
                if (newStepsData[i].functionId) newStepsData[i].functionId = customerRefData[0].functionMap[newStepsData[i].functionId]
                if (newStepsData[i].parameters && newStepsData[i].parameters.length) {
                    for (let j = 0; j < newStepsData[i].parameters.length; j++) {
                        delete newStepsData[i].parameters[j]._id
                        newStepsData[i].parameters[j].customerId = customerId
                        newStepsData[i].parameters[j].projectId = newStepsData[i].projectId
                        newStepsData[i].parameters[j].id = customerRefData[0].parameterMap[newStepsData[i].parameters[j].id]
                        if (newStepsData[i].parameters[j].moduleId) newStepsData[i].parameters[j].moduleId = newStepsData[i].moduleId
                    }
                }

            }
            // await dbManager.insertData("steps", newStepsData)
            // console.log("success")
            resolve(newStepsData)
        } catch (err) {
            reject(err)
        }
    })
}

ProcessData.prototype.fetchTestcases = async function (oldCustomerId, customerId, uniqueKey, userId) {
    let testcaseMap = {}
    let newTestcaseData = await dbManager.find("testcase", { customerId: oldCustomerId, deleted: false })
    let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
    for (let i = 0; i < newTestcaseData.length; i++) {
        delete newTestcaseData[i]._id
        newTestcaseData[i].createdBy = userId;
        newTestcaseData[i].updatedBy = userId;
        newTestcaseData[i].isDefault = true;
        newTestcaseData[i].createdOn = new Date()
        newTestcaseData[i].updatedOn = new Date()
        newTestcaseData[i].customerId = customerId
        newTestcaseData[i].projectId = customerRefData[0].projectMap[newTestcaseData[i].projectId]
        if (newTestcaseData[i].moduleId) newTestcaseData[i].moduleId = customerRefData[0].moduleMap[newTestcaseData[i].moduleId]
        // let data = await dbManager.getCurrentSequence("testcase")
        for (let j = 0; j < newTestcaseData[i].versions.length; j++) {
            let ver = newTestcaseData[i].versions[j].version
            let type = 'TC'
            // let parentId = data[0].seq+1
            let oldParentId = newTestcaseData[i].id
            let steps = await processDAta.fetchSteps(oldCustomerId, customerId, uniqueKey, type, ver, oldParentId)
            let tc
            if (!testcaseMap[newTestcaseData[i].id]) {
                let tempData = Object.assign({}, newTestcaseData[i]);
                delete tempData.id
                tc = await processDAta.handletcfn("testcase", { data: tempData, steps: steps })
                testcaseMap[newTestcaseData[i].id] = tc.id;
            } else {
                tc = await processDAta.handletcfn("testcase", { data: { copyFlag: false, diff_field: {}, id: testcaseMap[newTestcaseData[i].id], itsNewVersion: true, tdcopy: false, updateFields: {} }, steps: steps })
            }
        }
        // try {
        //     if (data.seq) {
        //         testcaseMap[newTestcaseData[i].id] = data.seq
        //         newTestcaseData[i].id = data.seq
        //         newTestcaseData[i].code = "TC-"+customerId+data.seq
        //     } 
        // }
        // catch (err) {
        //     console.log(err)
        // }
    }
    // await dbManager.update("customer_reference_data", { id: uniqueKey }, { $set: { testcaseMap: testcaseMap } })
    // await dbManager.insertData("testcase", newTestcaseData)
    console.log("success testcase")
    // processDAta.fetchSuites(oldCustomerId, customerId, uniqueKey, userId)
}

// ProcessData.prototype.fetchSuites = async function (oldCustomerId, customerId, uniqueKey, userId) {
//     let suitemap = {}
//     let newSuiteData = await dbManager.find("suites", { customerId: oldCustomerId, deleted: false })
//     let customerRefData = await dbManager.find("customer_reference_data", { id: uniqueKey });
//     for (let i = 0; i < newSuiteData.length; i++) {
//         delete newSuiteData[i]._id
//         newSuiteData[i].customerId = customerId
//         if (newSuiteData[i].projectId) newSuiteData[i].projectId = customerRefData[0].projectMap[newSuiteData[i].projectId]
//         newSuiteData[i].createdOn = new Date()
//         newSuiteData[i].updatedOn = new Date()
//         if (newSuiteData[i].createdBy) newSuiteData[i].createdBy = userId;
//         if (newSuiteData[i].updatedBy) newSuiteData[i].updatedBy = userId;
//         if (newSuiteData[i].testCases.length) {
//             for (let j = 0; j < newSuiteData[i].testCases.length; j++) {
//                 newSuiteData[i].testCases[j].customerId = customerId
//                 if (newSuiteData[i].testCases[j].moduleId) newSuiteData[i].testCases[j].moduleId = customerRefData[0].moduleMap[newSuiteData[i].testCases[j].moduleId]

//             }
//         }
//         let data = await dbManager.getNextSequence("suites")
//         try {
//             if (data.seq) {
//                 suitemap[newSuiteData[i].id] = data.seq
//                 newSuiteData[i].id = data.seq
//                 newSuiteData[i].code = "SU-" + customerId + data.seq

//             }
//         } catch (err) {
//             console.log(err)
//         }

//     }
//     await dbManager.insertData("suites", newSuiteData)
//     console.log("done suites")
// }



ProcessData.prototype.handletcfn = function (objectType, objectValue) {
    return new Promise(async (resolve, reject) => {
        try {

            let verData = {};
            verData.tdCopy = objectValue.data.tdcopy;
            verData.selver = objectValue.data.selVersion;
            // verData.isNewVersion =  objectValue.data.itsNewVersion;÷
            verData.copyFlag = objectValue.data.copyFlag
            delete objectValue.data.tdcopy;
            delete objectValue.data.selVersion;
            delete objectValue.data.itsNewVersion;
            delete objectValue.data.copyFlag
            let success = await dbManager.savetcfn(objectType, objectValue)
            let testDataConfigs = await dbManager.getasynQuery("componentConfig", { query: { customerId: objectValue.steps[0].customerId, deleted: false, name: "Environments" } })
            let environments = [];
            for (const i of testDataConfigs) {
                if (i.value[0].key == "environments") {
                    environments = i.value[0].values
                }
            }
            // cb(undefined, success.data)
            //if (objectType == "testcase" && success.data && success.data.customerId && success.data.projectId && success.data.moduleId && success.data.id) {
            // let query = { customerId: success.data.customerId, projectId: success.data.projectId, parentId: success.data.id, type: "TC" }
            //  let update = { $set: { moduleId: success.data.moduleId, updatedBy: success.data.updatedBy } }
            // await dbManager.asyncUpdate("steps", query, update);
            // }
            if (success.data.executionType && success.data.executionType == "MANUAL") {
                await dbManager.saveManualTestData(success.steps, success.data, environments)
            } else {
                await dbManager.testdatav2(success.steps, success.data, {}, environments, verData)
            }
            resolve(success.data)
        } catch (error) {
            // cb(error);
            reject(error);
        }
    }
    )


}




async function adduser(objValue, cb) {

    try {

        let customerCol = defaultAdd.customerCol(objValue.name, objValue.email, objValue["user-agent"], [1234]);

        let customer = await dbManager.saveObjectAsync('customer', customerCol);

        let projCol = defaultAdd.projCol('def_project', 'selenium', customer.id);

        let project = await dbManager.saveObjectAsync('project', projCol);

        let userCol = defaultAdd.userCol(customer.cust_name, customer.email, objValue.password, customer.id, project.id)

        let user = await dbManager.saveObjectAsync('user', userCol);

        let releaseCol = defaultAdd.releaseCol(user.customerId, user.id, user.code, user.projects[0], "PR-");

        let release = await dbManager.saveObjectAsync('release', releaseCol);

        let iterationCol = defaultAdd.iterationCol(release.customerId, release.id);

        let iteration = await dbManager.saveObjectAsync('iteration', iterationCol);

        let customerConfigCol = defaultAdd.customerConfigCol(user.customerId)

        let customerConfig =  await dbManager.saveObjectAsync('customerconfig', customerConfigCol)

        objValue.userId = user.id;

        delete objValue.password;


        cb(undefined, user);

        dbManager.saveadminDefault({ id: user.customerId })

    } catch (error) {

        cb(error);

    }

}



let labels = {

    "initiative": "Initiative",

    "userstory": "User Story",

    "iteration": "Iteration",

    "action": "Action",

    "branch": "Branch",

    "client": "Client",

    "defect": "Defect",

    "feature": "Feature",

    "function": "Function",

    "module": "Module",

    "objecttemplate": "Object Template",

    "object": "Object",

    "parameter": "Parameter",

    "project": "Project",

    "release": "Release",

    "role": "Role",

    "suites": "Suites",

    "task": "Task",

    "testcase": "Test Case",

    "testdata": "Test Data",

    "user": "User"

};

const defaultAdd = (function () {

    return {

        customerCol: function (name, email, useragent, packageId) {

            return {

                "cust_name": name,

                "admin_name": name,

                "companyName": name,

                "email": email,

                "license_type": "Customer Licensing",

                "license_count": 100,

                "fromdate": new Date(),

                "license_status": "Active",

                "user-agent": useragent,

                "packages": packageId,

               "packageList": [
                    {
                        "packageId" : packageId,
                        "noOfLicense" : 1000,
                        "licenseCost" : 100,
                        "start_date" : new Date()
                    }
                ],
                "startDate":new Date(),

                "labels": labels,

                "customerStatus": "Active",

                "durationInMonth": 24,

                "isAgile": true

            }

        },

        userCol: function (name, email, password, customerId, projectId) {

            return {
                "name": name,

                "email": email,

                "username": email,

                "password": password,

                "customerId": customerId,

                "usertype": "admin",

                "createdBy": customerId,

                "role": [

                    "project-lead"

                ],

                "projects": [

                    projectId

                ],

                ///"privileges":projectconfig.privileges,hgcgh

                "createdOn": new Date(),

                "dashboard": "PROJECT",

                "deleted": false,

                "active": true
            }

        },

        projCol: function (name, application, customerId) {

            return {

                "name": name,

                "application": application,

                "description": "this is default selenium project created",

                "default": true,

                "attributes": [

                    "ID",

                    "Xpath",

                    "Class",

                    "Css"

                ],

                "customerId": customerId,

                "createdBy": customerId,

                "deleted": false,

                "createdOn": new Date(),

            }

        },

        releaseCol: function (customerId, ownerId, ownerCode, projectId, projectCode) {

            return {

                "name": "def_release",

                "releasetype": "in-progress",

                "percent_storyplan": "0",

                "percent_storycount": "0",

                "feature_estimate": "100",

                "product_line": "No",

                "impact_appl": "Nothing",

                "release_indep": "No",

                "act_start_date": new Date(),

                "plan_start_date": new Date(),

                "default": true,

                "description": "Release of trail application",

                "customerId": customerId,

                "owner": [

                    ownerId

                ],

                "ownerCode": [

                    ownerCode

                ],

                "project": [

                    projectId

                ],

                "projectCode": [

                    projectCode

                ],

                "analyst": [

                    ownerId

                ],

                "createdBy": customerId,

                "deleted": false,

                "ownerId": customerId,

                "analystId": customerId,

                "createdOn": new Date()

            }

        },

        iterationCol: function (customerId, releaseId) {

            return {

                "name": "def_iteration",

                "percent_storyplan": "0",

                "percent_storycount": "0",

                "plan_start_date": new Date(),

                "description": "description of default iteration",

                "customerId": customerId,

                "createdBy": customerId,

                "deleted": false,

                "releaseId": releaseId,

                "createdOn": new Date(),

                "default": true

            }

        },
        customerConfigCol: function (customerId) {
            return {
                "userproxy": true,
                "customerId": customerId,
                "pointsOrTime": {
                    "storyPoints": true,
                    "time": false,
                    "dayValue": 7,
                    "timeValue": 8
                },
                "createdOn": new Date(),
                "enableJenkins": false,
                "jenkins": true,
                "passwordPolicy": {
                    "definedPassword": {
                        "upperCase": 1,
                        "lowerCase": 2,
                        "specialCharacter": 3,
                        "number": 2
                    },
                    "passwordExpiresIn": 90,
                    "remainderMail": 5,
                    "prePwdCount": 3,
                    "minPswdChar": 8,
                    "maxPswdChar": 24
                },
                "dashboard": [
                    {
                        "name": "Defect Density",
                        "value": "defectDensity",
                        "isTestcase": true,
                        "isSuite": true,
                        "isEcecplan": true
                    },
                    {
                        "name": "Total Test Case Execution Status Graph ",
                        "value": "totalTcExecStatus",
                        "isTestcase": true,
                        "isSuite": true,
                        "isEcecplan": true
                    },
                    {
                        "name": "Test Case Execution Status by Test Case Type ",
                        "value": "tcExecStatusByTcType",
                        "isTestcase": true,
                        "isSuite": true,
                        "isEcecplan": true
                    },
                    {
                        "name": "Test Case efficiency",
                        "value": "tcEfficiency",
                        "isTestcase": true,
                        "isSuite": true,
                        "isEcecplan": true
                    },
                    {
                        "name": "First Pass rate for execution ",
                        "value": "firstPassRateForExec",
                        "isEcecplan": true,
                        "isSuite": true,
                        "isTestcase": true
                    }
                ]
            }
        }
    };
})();






module.exports = ProcessData;
