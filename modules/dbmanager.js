// const db=require("./modules/dbconnection").getConnection("dburl");
var mongojs = require('mongojs');
const projectconfig = require('../config/projectconfig').get(process.env.NODE_ENV)
var db = mongojs(projectconfig["dburl"], []);
var to = require('../modules/to')
const uuid = require('uuid');

const DBManager = function(){}
var dbManager = new DBManager();



DBManager.prototype.findWithSkipLimit = async function(collection,query,skip,limit){
    return new Promise((resolve,reject)=>{
        db[collection].find(query).skip(skip).limit(limit).toArray((err,data)=>{
            // console.log(query)
            if(err)reject(err);
            else resolve(data);
        })
    })
}

DBManager.prototype.getasynQuerycondition = function (objType, objValue, condition, limit, skip, sort = {}) {
    return new Promise((resolve, reject) => {
        db[objType].find(objValue.query, condition).sort(sort).skip(skip).limit(limit).toArray(function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

DBManager.prototype.getasynQuery = function (objType, objValue) {
    return new Promise((resolve, reject) => {
        db[objType].find(objValue.query, function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

DBManager.prototype.find = async function(collection,query){
    return new Promise((resolve,reject)=>{
        db[collection].find(query,(err,data)=>{
            if(err)reject(err);
            else resolve(data);
        })
    })
}

DBManager.prototype.getData = function (objType, query, params = {}, callback) {
    db[objType].find(query, params, function (err, data) {

        if (err) callback(err);
        else callback(undefined, data);
    });
}

DBManager.prototype.create = async function(collection){
    return new Promise((resolve,reject)=>{
        db.createCollection(collection,(err,data)=>{
            if(err)reject(err);
            else console.log(`${collection} is created`);
        })
    })
}

DBManager.prototype.insertData = async function(collection,newData){
    return new Promise((resolve,reject)=>{
        db[collection].insert(newData,(err,data)=>{
            if(err)reject({data:newData,err:err});
            else resolve(data);
        })
    })
}

DBManager.prototype.saveObjectAsync = async function (objType, objVal) {
    return new Promise((resolve, reject) => {
        dbManager.saveObject(objType, objVal, function (err, data) {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

DBManager.prototype.getNextSequence = async function(collection){
    return new Promise((resolve,reject)=>{
        db.sequence.findAndModify({
            query:{"_id":collection},
            update:{ $inc:{"seq":1}},
            new:true,
            upsert:true
        },(err,data)=>{
            if(err)reject(err);
            else resolve(data);
        })
    })
}

DBManager.prototype.getCurrentSequence = async function(collection){
    return new Promise((resolve,reject)=>{
        db.sequence.find({ "_id":collection },(err,data)=>{
            if(err)reject(err);
            else resolve(data);
        })
    })
}

DBManager.prototype.count = async function(collection,query){
    return new Promise((resolve,reject)=>{
        db[collection].count(query,(err,data)=>{
            if(err)reject(err);
            else resolve(data);
        })
    })
}

DBManager.prototype.update = async function(collection,query,updateFields){
    return new Promise((resolve,reject)=>{
        db[collection].update(query,updateFields,(err,data)=>{
            if(err)reject(err);
            else resolve(data);
        })
    })
}

DBManager.prototype.saveadminDefault = function (objValue) {

    db["customerDefault"].find({}, function (err, data) {
        if(err){
            console.log(err)
        }else{
            for (let role of data[0].roles) {
                role.customerId = objValue.id;
            }
            dbManager.saveDataRecursively(data[0].roles, "role", {}, 0, function (err, val) {
                if (err) {

                    return;
                } else if (data[0].componentConfig && data[0].componentConfig.length) {
                    for (const config of data[0].componentConfig) {
                        config.customerId = objValue.id;
                        if(!config.value)continue;
                        for (const value of config.value) {
                            for (let val of value.values) {
                                if (!val.key) {
                                    val.key = objValue.id + "" + val.value;
                                }
                            }
                        }
                        
                    }
                    dbManager.saveDataRecursively(data[0].componentConfig, "componentConfig", {}, 0, function (err, val) {
                        return;
                    })
                } else {
                    return;
                }
            })
        }
    })
}


DBManager.prototype.saveDataRecursively = function (objValue, objType, map, i, cb) {

    if (i == objValue.length) {

        cb(undefined, map)
        return;
    }
    dbManager.saveObject(objType, objValue[i], function (err, val) {

        if (err) {
            cb(err);
        } else {
            if (!map[objValue[i].name]) {
                map[objValue[i].name] = val.id;
            }

            dbManager.saveDataRecursively(objValue, objType, map, i + 1, cb);
        }
    })
}




DBManager.prototype.asyncUpdate = function (objtype, query, value) {
    return new Promise((resolve, reject) => {
        db[objtype].update(query, value, { multi: true }, function (err, d) {
            if (err) reject(err)
            else resolve(d)
        })
    })
}

DBManager.prototype.updateMany = async function(collection,query,updateFields){
    return new Promise((resolve,reject)=>{
        db[collection].updateMany(query,updateFields,(err,data)=>{
            if(err)reject(err);
            else resolve(data);
        })
    })
}


DBManager.prototype.savetcfn = function (objectType, objectValue) {
    return new Promise(async (resolve, reject) => {
        //For concurrency issue
        if (objectValue.data.id) {
            let query = '';
            if (objectType == "testcase") {
                query = { testcaseId: objectValue.data.id, customerId: objectValue.steps[0].customerId, projectId: objectValue.steps[0].projectId, version: objectValue.steps[0].version }
            } else {
                query = { functionId: objectValue.data.id, customerId: objectValue.steps[0].customerId, projectId: objectValue.steps[0].projectId }
            }
            let latestTestdata = await dbManager.find("testdata", { query: query });
            if (!latestTestdata || !latestTestdata.length || !latestTestdata[0].recordVersion) {
                latestTestdata = [{ recordVersion: 0 }];
            }
            await dbManager.asyncUpdate("testdata", query, { $set: { recordVersion: parseInt(latestTestdata[0].recordVersion) + 1 } });
        }

        // if (objectType == "testcase" && objectValue.data.id) {
        //     let testcase;
        //     try {
        //         testcase = await dbManager.find("testcase", { query: { id: objectValue.data.id, customerId: objectValue.steps[0].customerId, projectId: objectValue.steps[0].projectId } })
        //         if (!testcase[0].typeId && objectValue.data.updateFields.typeId) {
        //             // adding first time
        //             // rabbitmq.publishq('', 'testcase', { purpose: "ADD", data: { ...objectValue.data.updateFields, customerId: testcase[0].customerId, projectId: testcase[0].projectId, testcaseId: testcase[0].id, executionType: testcase[0].executionType } })
        //         } else if (objectValue.data.updateFields.typeId && testcase[0].typeId && objectValue.data.updateFields.typeId != testcase[0].typeId) {
        //             // remove from old and insert in new
        //             let data1 = { purpose: "ADDREMOVE", data: { ...objectValue.data.updateFields, customerId: testcase[0].customerId, projectId: testcase[0].projectId, testcaseId: testcase[0].id, executionType: testcase[0].executionType }, oldtypeId: testcase[0].typeId }
        //             // rabbitmq.publishq('', 'testcase', data1)
        //         } else if (!objectValue.data.updateFields.typeId && testcase[0].typeId) {
        //             // remove the existing testcase from suite
        //             objectValue.data.updateFields["typeId"] = undefined;
        //             // rabbitmq.publishq('', 'testcase', { purpose: "REMOVE", data: { ...objectValue.data.updateFields, customerId: testcase[0].customerId, projectId: testcase[0].projectId, testcaseId: testcase[0].id, executionType: testcase[0].executionType } })
        //         }
        //     } catch (error) {
        //         reject(error);
        //     }
        // }
        if (objectValue.data.versions) {
            let type = typeof objectValue.data.versions[0];
            if (type == "number") {
                let versions = [{ version: 1, name: "v1", deleted: false, createdBy: objectValue.data.createdBy }]
                objectValue.data.versions = versions;
            }
        }
        //testcase should be of recorded(either mobile, mainframe, record and playback))
        if (objectType == "testcase" && !objectValue.data.id && objectValue.data.executionType != "MANUAL") {
            let allparameters = [];
            for (let step of objectValue.steps) {
                if (step.parameters) {
                    for (let i = 0; i < step.parameters.length; i++) {
                        if (!step.parameters[i].id) {
                            allparameters.push(new RegExp("^" + step.parameters[i].name + "$", "i"))
                        }
                    }
                }
            }
            if (allparameters.length) {
                try {
                    let parameters = await dbManager.getasynQuerycondition("parameter", { "query": { "customerId": objectValue.data.customerId, "projectId": objectValue.data.projectId, "deleted": false, "name": { $in: allparameters } } }, { "id": 1, "name": 1, "paramtype": 1, "code": 1, "paramAttributes": 1 }, allparameters.length, 0)
                    let paramMap = {}
                    if (parameters.length) {
                        for (let param of parameters) {
                            paramMap[param.name.toLowerCase()] = param;
                        }
                    }


                    for (let step of objectValue.steps) {
                        for (let param of step.parameters) {
                            let defaultValue;
                            if(param.id&&!paramMap[param.name.toLowerCase()]){
                                    paramMap[param.name.toLowerCase()] = param;
                                    if(param.defaultValue)
                                    defaultValue = param.defaultValue
                            }
                            if (!paramMap[param.name.toLowerCase()]) {    
                                //if not found then create in db and save
                               let parameter= await new Promise((resolve,reject)=>{
                                   defaultValue = param.defaultValue
                                   delete param.defaultValue;
                                    dbManager.saveObject("parameter",{...param,paramtype:"Alphanumeric",type:"LOCAL",deleted:false,customerId:step.customerId,projectId:step.projectId},function(err,data){
                                        if(err){

                                        } else {
                                            resolve(data)
                                        }
                                    })
                                })
                                paramMap[parameter.name.toLowerCase()] = parameter
                            }
                            param.id = paramMap[param.name.toLowerCase()].id;
                            if(defaultValue)
                                param.defaultValue = defaultValue
                            param.name = paramMap[param.name.toLowerCase()].name;
                            param.code = paramMap[param.name.toLowerCase()].code;
                            param.paramtype = paramMap[param.name.toLowerCase()].paramtype;
                            param.paramAttributes = paramMap[param.name.toLowerCase()].paramAttributes;

                        }
                    }
                } catch (error) {
                    return reject(error)
                }

            }
        }

        dbManager.saveObject(objectType, objectValue.data, function (err, data) {
            if (err) {
                reject(err);
            } else {
                let type;
                let parentId = data.id;
                if (objectType == "functions") {
                    type = "FN";
                } else if (objectType == "testcase") {
                    type = "TC";
                }
                if(objectValue.steps && objectValue.steps.length){
                for (let i = 0; i < objectValue.steps.length; i++) {
                    objectValue.steps[i].type = type;
                    objectValue.steps[i].parentId = parentId;
                    if (objectValue.steps[i].children && objectValue.steps[i].children.length) {
                        for (let j = 0; j < objectValue.steps[i].children; j++) {
                            objectValue.steps[i].children[j].type = type;
                            objectValue.steps[i].children[j].parentId = parentId;
                            objectValue.steps[i].children[j].parentStepId = objectValue.steps[i].id;
                        }
                    }
                }
            }
                dbManager.saveBulkObjects('steps', objectValue, function (err, success) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({ "data": data, "steps": objectValue.steps })
                    }
                })
            }
        })
    })
}


DBManager.prototype.saveManualTestData = function (objValue, testcase, environments) {
    dbManager.getData("testdata_environments", { testcaseId: objValue[0].parentId, customerId: objValue[0].customerId, projectId: objValue[0].projectId, version: objValue[0].version }, async function (err, data) {
        if (err) {
            console.log(err)
            console.log("line number 1639")
        } else if (data.length) {
            for (let td of data) {
                let prvTestData = td.iterations;
                let stepIds = prvTestData[0].data.map((obj) => { return obj.stepId });
                let newTestData = [];
                for (let i = 0; i < prvTestData.length; i++) {
                    newTestData.push({ itrId: prvTestData[i].itrId, name: "", data: [] })
                }
                for (let i = 0; i < objValue.length; i++) {
                    let si = stepIds.indexOf(objValue[i].id)
                    if (si > -1 && prvTestData[0].data[si].name == objValue[i].parameter) {
                        for (let j = 0; j < prvTestData.length; j++) {
                            newTestData[j].data.push(prvTestData[j].data[si]);
                        }
                    } else if (objValue[i].parameter && objValue[i].parameter.trim().length) {
                        for (let j = 0; j < prvTestData.length; j++) {
                            newTestData[j].data.push({ name: objValue[i].parameter, value: "", type: "MANUAL", stepId: objValue[i].id, stepSeq: objValue[i].seq })
                        }
                    }
                }
                if (td.environmentType == 'general') {
                    [err1, data1] = await to(dbManager.saveObjectAsync("testdata", JSON.parse(JSON.stringify({ id: td.parentId, customerId: td.customerId, updateFields: { moduleId: objValue[0].moduleId } }))))
                }
                [err1, data1] = await to(dbManager.saveObjectAsync("testdata_environments", JSON.parse(JSON.stringify({ id: td.id, environmentType: td.environmentType, tdData: td, updateFields: { moduleId: objValue[0].moduleId, iterations: newTestData } }))))
                if (err1) break;
            }

        } else {
            //Math.random().toString(26).substr(5, 6);
            let versionName;
            for (let i = 0; i < testcase.versions.length; i++) {
                if (objValue[0].version == testcase.versions[i].version) {
                    versionName = testcase.versions[i].name;
                    break;
                }
            }
            let testdata = { "moduleId": objValue[0].moduleId, "projectId": objValue[0].projectId, "executionType": "MANUAL", "customerId": objValue[0].customerId, "testcaseId": objValue[0].parentId, deleted: false, createdBy: objValue[0].createdBy, version: objValue[0].version, versionName: versionName,type:objValue[0].type }
            testdata.iterations = [{ itrId: Math.random().toString(26).substr(5, 6), name: "", data: [] }]
            testdata.defaultIteration = testdata.iterations[0].itrId;
            for (let i = 0; i < objValue.length; i++) {
                if (objValue[i].parameter && objValue[i].parameter.trim().length) {
                    let itrObj = { name: objValue[i].parameter, value: "", type: "MANUAL", stepId: objValue[i].id, stepSeq: objValue[i].seq }
                    testdata.iterations[0].data.push(itrObj)
                }
            }
            let err1, data1, generalTDId
            for (const i of environments) {
                testdata.environmentType = i.key;
                if (i.key == 'general') {
                    let tempData = JSON.parse(JSON.stringify(testdata))
                    tempData.type= "TC"
                    delete tempData.iterations;
                    delete tempData.environmentType;
                    delete tempData.defaultIteration;
                    [err1, data1] = await to(dbManager.saveObjectAsync("testdata", JSON.parse(JSON.stringify(tempData))))
                    generalTDId = JSON.parse(JSON.stringify(data1.id))
                }
                testdata.parentId = generalTDId;
                [err1, data1] = await to(dbManager.saveObjectAsync("testdata_environments", JSON.parse(JSON.stringify(testdata))))
                if (err1) {
                    break;
                }
            }
        }
    })
}

DBManager.prototype.testdatav2 =  function (steps, from, data, environments, verData) {
    // if(verData.isNewVersion){
    //     delete from.copiedFrom;
    // }
    return new Promise(async (resolve,reject)=>{
        data.projectId = from.projectId;
        data.customerId = from.customerId;
        data.deleted = false;
        data.moduleId = from.moduleId;
        let funcIdCode = [];
        let set1 = new Set();
        for (let step of steps) {
            if (step.functionId) set1.add(step.functionId)
        }
        let functionIds = [...set1];
        let functionMap = {};
        try {
            let allEnvironments = []
            data.functionIds = functionIds;
            for (let env of environments) {
                allEnvironments.push(env.key)
            }
            (functionIds.length) ? functionMap = await dbManager.getfunctionMap(data, allEnvironments) : functionMap = {};
    
            from.versions= from.versions.filter((ele) =>{return !ele.deleted})
            var parentId;
            from.versions= from.versions.filter((ele) =>{return !ele.deleted})
            for (let td of environments) {
                data.functionIds = functionIds;
                let newIteration = [];
                let parameters = [];
                for (const element of steps) {
                    if (!element.functionId) {
                        if (element.parameters && element.parameters.length) {
                            for (const params of element.parameters) {
                                if (parameters.indexOf(params.name) != -1) {
                                    continue;
                                }
                                parameters.push(params.name);
                                if (params.defaultValue && params.defaultValue.length) {
                                    let obj = {
                                        name: params.name,
                                        ptype: (params.type && params.id) ? params.type : "LOCAL",
                                        paramId: (params.id) ? params.id : undefined,
                                        value: params.defaultValue,
                                        stepId: element.id,
                                        stepSeq: element.seq,
                                        type: "OB"
                                    };
                                    if (params.paramtype && params.paramAttributes) {
                                        obj.paramtype = params.paramtype;
                                        obj.paramAttributes = params.paramAttributes;
                                    }
                                    if (params.protected) {
                                        obj.protected = true;
                                        obj.value = crypto.getencrypted(params.defaultValue);
                                        obj.isEncrypted = true;
                                    }
                                    newIteration.push(obj)
                                } else {
                                    let obj = {
                                        name: params.name,
                                        ptype: (params.type && params.id) ? params.type : "LOCAL",
                                        paramId: (params.id) ? params.id : undefined,
                                        value: "",
                                        stepId: element.id,
                                        stepSeq: element.seq,
                                        type: "OB"
                                    }
                                    if (params.protected) {
                                        obj.protected = true;
                                        obj.isEncrypted = false;
                                    }
                                    if (params.paramtype && params.paramAttributes) {
                                        obj.paramtype = params.paramtype;
                                        obj.paramAttributes = params.paramAttributes;
                                    }
                                    newIteration.push(obj)
                                }
                            }
                        }
                    } else {
                        let obj = {
                            name: element.functionCode,
                            functionId: element.functionId,
                            stepId: element.id,
                            iterationSelected: [functionMap[`${td.key}-${element.functionId}-${element.fnVersion}`].defaultIteration],
                            testdataId: functionMap[`${td.key}-${element.functionId}-${element.fnVersion}`].id,
                            version:element.fnVersion,
                            type: "FN",
                        };
                        newIteration.push(obj)
                    }
                    if (element.children && element.children.length) {
                        var paramnames = parameters;
                        for (const child of element.children) {
                            if (child.parameter1 && child.parameter1.length && child.parameter2 && child.parameter2.length) {
                                let param1 = child.parameter1[0];
                                let param2 = child.parameter2[0];
                                if (paramnames.indexOf(param1.name) < 0) {
                                    paramnames.push(param1.name);
                                    if (param1.defaultValue && param1.defaultValue.length) {
                                        let obj = {
                                            name: param1.name,
                                            value: param1.defaultValue,
                                            ptype: (param1.type && param1.id) ? param1.type : "LOCAL",
                                            paramId: (param1.id) ? param1.id : undefined,
                                            stepId: element.id,
                                            childStepId: child.id,
                                            childStepSeq: child.seq,
                                            stepSeq: child.seq,
                                            type: "OB"
                                        }
                                        if (param1.protected && !param1.isEncrypted) {
                                            obj.protected = true;
                                            obj.value = crypto.getencrypted(param1.defaultValue);
                                            obj.isEncrypted = true;
                                        }
                                        newIteration.push(obj)
                                    }
                                    else {
                                        let obj = {
                                            name: param1.name,
                                            value: "",
                                            ptype: (param1.type && param1.id) ? param1.type : "LOCAL",
                                            paramId: (param1.id) ? param1.id : undefined,
                                            stepId: element.id,
                                            childStepId: child.id,
                                            childStepSeq: child.seq,
                                            stepSeq: element.seq,
                                            type: "OB"
                                        }
                                        if (param1.protected) {
                                            obj.protected = true;
                                            obj.isEncrypted = false;
                                        }
                                        newIteration.push(obj);
                                    }
                                }
                                if (paramnames.indexOf(param2.name) < 0) {
                                    paramnames.push(param2.name);
                                    if (param2.defaultValue && param2.defaultValue.length) {
                                        let obj = {
                                            name: param2.name,
                                            value: param2.defaultValue,
                                            ptype: (param2.type && param2.id) ? param2.type : "LOCAL",
                                            paramId: (param2.id) ? param2.id : undefined,
                                            stepId: element.id,
                                            childStepId: child.id,
                                            childStepSeq: child.seq,
                                            stepSeq: child.seq,
                                            type: "OB"
                                        };
                                        if (param2.protected && !param2.isEncrypted) {
                                            obj.protected = true;
                                            obj.value = crypto.getencrypted(param2.defaultValue);
                                            obj.isEncrypted = true;
                                        }
                                        newIteration.push(obj)
                                    }
                                    else {
                                        let obj = {
                                            name: param2.name,
                                            value: "",
                                            ptype: (param2.type && param2.id) ? param2.type : "LOCAL",
                                            paramId: (param2.id) ? param2.id : undefined,
                                            stepId: element.id,
                                            childStepId: child.id,
                                            childStepSeq: child.seq,
                                            stepSeq: element.seq,
                                            type: "OB"
                                        };
                                        if (param2.protected) {
                                            obj.protected = true;
                                            obj.isEncrypted = false;
                                        }
                                        newIteration.push(obj);
                                    }
                                }
                            }
                            if (child.parameters && child.parameters.length) {
                                for (const params of child.parameters) {
                                    if (paramnames.indexOf(params.name) != -1) {
                                        continue;
                                    }
                                    paramnames.push(params.name);
                                    if (params.defaultValue && params.defaultValue.length) {
                                        let obj = {
                                            name: params.name,
                                            ptype: (params.type && params.id) ? params.type : "LOCAL",
                                            paramId: (params.id) ? params.id : undefined,
                                            value: params.defaultValue,
                                            stepId: element.id,
                                            stepSeq: element.seq,
                                            type: "OB"
                                        };
                                        if (params.paramtype && params.paramAttributes) {
                                            obj.paramtype = params.paramtype;
                                            obj.paramAttributes = params.paramAttributes;
                                        }
                                        if (params.protected) {
                                            obj.protected = true;
                                            obj.value = crypto.getencrypted(params.defaultValue);
                                            obj.isEncrypted = true;
                                        }
                                        newIteration.push(obj)
                                    } else {
                                        let obj = {
                                            name: params.name,
                                            ptype: (params.type && params.id) ? params.type : "LOCAL",
                                            paramId: (params.id) ? params.id : undefined,
                                            value: "",
                                            stepId: element.id,
                                            stepSeq: element.seq,
                                            type: "OB"
                                        }
                                        if (params.protected) {
                                            obj.protected = true;
                                            obj.isEncrypted = false;
                                        }
                                        if (params.paramtype && params.paramAttributes) {
                                            obj.paramtype = params.paramtype;
                                            obj.paramAttributes = params.paramAttributes;
                                        }
                                        newIteration.push(obj)
                                    }
                                }
                            }
                        }
                    }
                }
                let versionName;
                let lastversion = 0;
                if (steps[0].version) {
                    if (steps[0].version == 1) {
                        if(verData.selver){
                            // copying from testcase to function 
                            data.version = verData.selver;
                        }else{
                            data.version = 1;
                        }
                        versionName = from.versions[0].name;
                    } 
                    else {
                        // let indexVersion=-1;            
                        if(verData.selver){
                            lastversion = verData.selver;
                            for (let i = 0; i < from.versions.length; i++){
                                if(steps[0].version == from.versions[i].version){
                                    versionName = from.versions[i].name;
                                    break;
                                }
                            }
                        } else{
                            for (let i = 0; i < from.versions.length; i++) {
                                if (steps[0].version == from.versions[i].version) {
                                    while (i != 0) {
                                        if (!from.versions[i - 1].deleted) {
                                            lastversion = from.versions[i - 1].version;
                                            break;
                                        }
                                        i--;
                                    }
                                    versionName = from.versions[i].name;
                                    break;
                                }
                            }
                        }
                        data.version = {
                            $in: [steps[0].version, lastversion]
                        }       
                    }                       
                } else {
                    data.version = 1;
                    versionName = 1;
                }
                delete data.functionIds;
                if (steps[0].type == 'TC') {
                    data.testcaseId = steps[0].parentId; data.type = "TC";
                } else {
                    data.functionId = steps[0].parentId; data.type = "FN";
                }
                data.environmentType = td.key;
                let [err1, data1] = await to(dbManager.saveTdforEachEnv(JSON.parse(JSON.stringify(data)), from, functionMap, newIteration, versionName, td.key, parentId, steps[0], verData))
                if (err1) break;
                parentId = data1
            }
            deleteDefalutValForLocalParams(data, steps);
            resolve()
    
        } catch (error) {
            reject(error)
        }
    })
   
}

function setDataType(val, obj) {
    var result = undefined;
    switch (val.type) {
        case "Date":
            result = new Date(obj);
            break;

        default:
            result = obj;
            break;
    }
    return result;
}

DBManager.prototype.createDefaultForProject = async function (ObjValue) {
    let defsuites = [
        { "name": "Regression", "description": "Default Regression", "deleted": false, "testCases": [], "isDefault": true },
        { "name": "Smoke", "description": "Default Smoke", "deleted": false, "testCases": [], "isDefault": true },
        { "name": "Functional", "description": "Default Functional", "deleted": false, "testCases": [], "isDefault": true },
        { "name": "Sanity", "description": "Default Sanity", "deleted": false, "testCases": [], "isDefault": true }]
    for (let suite of defsuites) {
        suite.projectId = ObjValue.id;
        suite.customerId = ObjValue.customerId;
        await dbManager.saveasync("suites", suite)
    }
}

DBManager.prototype.saveasync = function (objType, objVal) {
    return new Promise((resolve, reject) => {
        // objectManager.getNextSequence(objType, function(err, id) {
        //     if (err) {
        //         reject(err);
        //     } else {
        //         objVal.id = id.seq;
        //         delete objVal._id;
        //         db[objType].insert(objVal, function(err, data) {
        //             if (err) {
        //                 reject(err);
        //             } else {
        //                 resolve(data);
        //             }
        //         })
        //     }
        // })
        delete objVal._id;
        dbManager.saveObject(objType, objVal, function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}


DBManager.prototype.saveObject = async function (objType, objValue, cb) {
    if (objValue.customDataType && objValue.customDataType.length) {
        if (objValue.updateFields) {
            for (var vari of objValue.customDataType) {
                objValue.updateFields[vari.variable] = setDataType(vari, objValue.updateFields[vari.variable]);
            }
        } else {
            for (var vari of objValue.customDataType) {
                objValue[vari.variable] = setDataType(vari, objValue[vari.variable]);
            }
        }
    }
    let fromJira = objValue.fromJira
    delete objValue.fromJira
    if (objValue.id == undefined) {
        objValue.createdOn = new Date();
        objValue.updatedOn = new Date();

        dbManager.getNextSequence1(objType,async function (err, id) {
            let ranNum = uuid.v4()
            if (err) {
                cb(err);
            } else {
                if (objType != "testdata_envinroments") {
                    objValue.id = id.seq;
                }
                switch (objType) {
                    case "module":
                        objValue.code = "MD-" + objValue.customerId + id.seq;
                        break;
                    case "project":
                        objValue.code = "PR-" + objValue.customerId + id.seq;
                        break;

                    case "testcase":
                        objValue.code = "TC-" + objValue.customerId + id.seq;
                        break;
                    case "testdata":
                        if (objValue.type == "TC" || objValue.type == "MANUAL") {
                            objValue.code = "TCD-" + objValue.customerId + id.seq;
                        } else if (objValue.type == 'SU') {
                            objValue.code = "TSD-" + objValue.customerId + id.seq;
                        } else {
                            objValue.code = "TFD-" + objValue.customerId + id.seq;
                        }
                        break;
                    case "functions":
                        objValue.code = "FN-" + objValue.customerId + id.seq;
                        break;

                    case "parameter":
                        if (objValue.protected && objValue.type == 'GLOBAL') {
                            objValue.defaultValue = crypto.getencrypted(objValue.defaultValue);
                            objValue.isEncrypted = true;
                        }
                        objValue.code = "PM-" + objValue.customerId + id.seq;
                        break;
                    case "object":
                        objValue.code = "OB-" + objValue.customerId + id.seq;
                        break;
                    case "defect":
                        objValue.code = "DE-" + objValue.customerId + id.seq;
                        break;
                    case "suites":
                        objValue.code = "SU-" + objValue.customerId + id.seq;
                        break;
                    case "release":
                        objValue.code = "RE-" + objValue.customerId + id.seq;
                        break;
                    case "user":
                        objValue.code = "UR-" + objValue.customerId + id.seq;
                        break;
                    case "iteration":
                        objValue.code = "IT-" + objValue.customerId + id.seq;
                        break;
                    case "userstory":
                        objValue.code = "US-" + objValue.customerId + id.seq;
                        break;
                    case "defects":
                        objValue.code = "DF-" + objValue.customerId + id.seq;
                        break;
                    case "execution":
                        objValue.code = "EX-" + objValue.customerId + id.seq;
                        objValue.deleted = false;
                        break;
                    case "initiative":
                        objValue.code = "IN-" + objValue.customerId + id.seq;
                        break;
                    case "feature":
                        objValue.code = "FT-" + objValue.customerId + id.seq;
                        break;
                    case "task":
                        objValue.code = "TA-" + objValue.customerId + id.seq;
                        break;
                    case "team":
                        objValue.code = "TM-" + objValue.customerId + id.seq;
                        break;
                    case "user_prefernce":
                        objValue.code = "UP-" + objValue.customerId + id.seq;
                        break;
                    case "client":
                        objValue.code = "CL-" + objValue.customerId + id.seq;
                        break;
                    case "branch":
                        objValue.code = "BR-" + objValue.customerId + id.seq;
                        break;
                    case "queries":
                        objValue.code = "QU-" + objValue.customerId + id.seq;
                        break;
                    case "executionplan":
                        objValue.code = "EP-" + objValue.customerId + id.seq;
                        break;
                    case "scheduler":
                        objValue.code = "SH-" + objValue.customerId + id.seq;
                        break;
                    case "jarupload":
                        objValue.code = "JAR-" + objValue.customerId + id.seq;
                        break;
                    case "pipeline":
                        objValue.code = "PL-" + objValue.customerId + id.seq;
                        break;
                    case "testdata_environments":
                        objValue.id =ranNum
                }
                delete objValue.customDataType;
                let gitlab_comment = false;
                let gitlaab_Comments;
                db[objType].save(objValue, async function (err, saved) {
                    if (err || !saved) {
                        cb(err);
                    } else {
                        // cb(undefined, saved);
                        switch (objType) {

                            case 'project':
                                dbManager.createDefaultForProject(saved)
                                break;
                        }
                        cb(undefined, saved);
                        // if (saved.notify) {
                        //     saved.notifyPurpose = 'NEW';
                        //     if (projectconfig["sendmail"]) {
                        //         saved.objType = objType.charAt(0).toUpperCase() + objType.slice(1);
                        //         // rabbitmq.publishq('', 'mailnotify', { objType: objType, objValue: saved })
                        //     }
                        //     dbManager.sendNotification(objType, saved);
                        // }
                        // if (avoidHistory.indexOf(objType) < 0) {
                        //     var objTypeHistory = objType + "_history";
                        //     dbManager.saveObjectHistory(objTypeHistory, objValue, function (err, done) {
                        //         if (err) {
                        //             console.log("history oject not saved");
                        //         }
                        //     });
                        // }
                    }
                });
            }
        })
    } else {
        var tmpId;
        if (objType == "execplantestcases" || objType == "testdata_environments") {
            tmpId = objValue.id;
        } else {
            tmpId = parseInt(objValue.id);
        }
        var updateFields = objValue.updateFields;
        var diff_fields = {}
        if(objValue.diff_field) diff_fields = objValue.diff_field;
        updateFields.updatedOn = new Date();
        delete objValue.customDataType;
        if (objType == "suites" && updateFields.testcaseFromDefaultSuite && updateFields.testcaseFromDefaultSuite.length) {
            dbManager.asyncUpdate("testcase", { id: { "$in": updateFields.testcaseFromDefaultSuite } }, { $set: { typeId: null, typeName: null } })
            delete updateFields.testcaseFromDefaultSuite;
        } else if (objType == "testdata_environments") {
            if (updateFields.iterations && updateFields.iterations.length) {
                updateFields.iterations = dbManager.updateencryptTD(updateFields.iterations, updateFields);
            }
            // if(updateFields.newlyaddedItrs.length){
            //     updateFields.newlyaddedItrs = dbManager.updateencryptTD(updateFields.newlyaddedItrs);
            // }
        } else if (objType == "parameter") {
            if (!updateFields.isEncrypted && updateFields.protected && updateFields.defaultValue && updateFields.defaultValue.length) {
                updateFields.defaultValue = crypto.getencrypted(updateFields.defaultValue);
                updateFields.isEncrypted = true;
            }
        } else if (objType == "componentConfig") {
            if (objValue.updateFields.value && objValue.updateFields.value[0].key == "environments") {
                let [err1, data] = await to(dbManager.createDefaultTestData(objValue))
                if (err1) {
                    cb(err1)
                    return
                }
            }
        }else if(objType == "execplantestcases"){
            if(objValue.updateFields.executionstarted){
                objValue.updateFields.executionstarted = new Date();
            }
        }
        let gitlab_comment = false;
        let gitlaab_Comments;       
        var comment = false;
        let qry;
        if(objType == "testdata_environments" && objValue.tdData) qry = {id: tmpId , customerId: objValue.tdData.customerId ,projectId: objValue.tdData.projectId ,deleted: false ,environmentType: objValue.tdData.environmentType, moduleId: objValue.tdData.moduleId ,parentId: objValue.tdData.parentId ,testcaseId: objValue.tdData.testcaseId}
        else qry = { id: tmpId };
        db[objType].findAndModify({
            query: qry,
            update: { $set: updateFields },
            new: true
        }, async function (err, doc, lastErrorObject) {
            if (err) {
                cb(err);
            } else {
                // if (objType == "defect") {
                //     gitlabintegration.gitlabDefect(objType, doc, false, dbManager);
                // }
                objValue.id = tmpId;
                // cb(undefined, JSON.parse(JSON.stringify(doc)));
                let comments;
                switch (objType) {
                                            
                    // case 'defect':
                    //     if (process.env.NODE_ENV != 'production' && doc.projectId == 8166 && doc.customerId == 3356) rabbitmq.publishq('', 'UpdatejiraDefect', doc);
                    //     break;
                }
                if (gitlab_comment && comments && comments.length) {
                    cb(undefined, JSON.parse(JSON.stringify(comments)));
                } else {
                    cb(undefined, JSON.parse(JSON.stringify(doc)));
                }
                if (doc && doc.notify&&((diff_fields&&Object.keys(diff_fields).length!=0)||comment)) {
                    doc.diff_fields = diff_fields;
                    doc.notifyPurpose = 'UPDATE';
                    if (projectconfig["sendmail"]) {
                       doc.objType = objType.charAt(0).toUpperCase() + objType.slice(1);
                        // rabbitmq.publishq('', 'mailnotify', { objType: objType, objValue: doc,comment:comment  })
                    }
                    // dbManager.sendNotification(objType, doc);

                }
                // if (avoidHistory.indexOf(objType) < 0) {
                //     if (!doc) {
                //         return;
                //     }
                //     delete doc._id;
                //     var objTypeHistory = objType + "_history";

                //     dbManager.saveObjectHistory(objTypeHistory, doc, function (err, done) {
                //         if (err) {
                //             console.log("history oject not saved");
                //         } else {

                //         }
                //     });
                // }
            }
        });
    }
}

DBManager.prototype.getNextSequence1 = function (objValue, cb) {
    db.sequence.findAndModify({
        query: { "_id": objValue },
        update: { $inc: { "seq": 1 } },
        new: true,
        upsert: true
    }, function (err, doc) {
        cb(err, doc);
    });
}

DBManager.prototype.saveBulkObjWithCount = function (objectType, objValue, count, cb) {
    var bulk = db[objectType].initializeUnorderedBulkOp();
    var bulk_history = db[objectType + "_history"].initializeUnorderedBulkOp();
    for (var i in objValue) {
        if (objValue[i].functionId) {
            objValue[i].objectId = undefined;
        } else {
            objValue[i].functionId = undefined;
        }
        if (objValue[i].id == undefined) {
            objValue[i].id = "" + objValue[i].moduleId + objValue[i].projectId + objValue[i].customerId + Math.random().toString(26).substr(4, 8) + count;
            if (objValue[i].children && objValue[i].children) {
                for (const j in objValue[i].children) {
                    objValue[i].children[j].id = "" + objValue[i].id + Math.random().toString(26).substr(4, 6) + j;
                    objValue[i].children[j].createdOn = new Date();
                }
            }
            count++;
            objValue[i].createdOn = new Date();
            bulk.insert(objValue[i]);
            bulk_history.insert(objValue[i]);
        } else {
            delete objValue[i]._id;
            objValue[i].updatedOn = new Date();
            if (objValue[i].children && objValue[i].children.length) {
                for (const k in objValue[i].children) {
                    if (objValue[i].children[k].id == undefined) {
                        objValue[i].children[k].id = "" + objValue[i].id + Math.random().toString(26).substr(4, 6);
                        objValue[i].children[k].createdOn = new Date();
                    } else {
                        objValue[i].children[k].updatedOn = new Date();
                    }
                }
            }
            bulk.find({ id: objValue[i].id, customerId: objValue[i].customerId, projectId: objValue[i].projectId, parentId: objValue[i].parentId }).update({ $set: objValue[i] });
            bulk_history.insert(JSON.parse(JSON.stringify(objValue[i])));
        }
    }
    bulk.execute(function (err, result) {
        if (err || !result) {
            console.log("New Object not saved");
            cb(err);
        } else {
            cb(undefined, result);
        }
    });
    bulk_history.execute(function (err, result) {
        if (err || !result) {
            console.log("New Object not saved");

        } else { }
    });
}

DBManager.prototype.saveBulkObjects = function (objType, objValue, cb) {
    var query = {
        moduleId: objValue.steps[0].moduleId,
        projectId: objValue.steps[0].projectId,
        customerId: objValue.steps[0].customerId,
        parentId: objValue.steps[0].parentId,
        type: objValue.steps[0].type
    };
    db.steps.count(query, function (err, count) {
        if (err) {
            cb(err);
        } else {
            dbManager.saveBulkObjWithCount(objType, objValue.steps, count, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    cb(undefined, data);
                }
            });
        }
    });
}


DBManager.prototype.getfunctionMap = async function (data, allenvironments) {
    return new Promise((resolve, reject) => {
        dbManager.getData('testdata_environments', { environmentType: { $in: allenvironments }, customerId: data.customerId, projectId: data.projectId, deleted: false, functionId: { $in: data.functionIds } }, function (err, functiondata) {
            if (err) {
                reject(err)
            } else {
                let functionMap = {};
                functiondata.forEach(element => {
                    functionMap[`${element.environmentType}-${element.functionId}-${element.version}`] = element;
                });
                resolve(functionMap);
            }
        })
    })
}

DBManager.prototype.saveTdforEachEnv = function (data, from, functionMap, newIteration, versionName, environmentType, parentId, step, verData) {
    return new Promise((resolve, reject) => {
        try {
            if (data.createdBy) delete data.createdBy;
            delete data.moduleId;
            let tdQuery = Object.assign({}, data)
            // if(from.copiedFrom) verData.copiedFromId = from.copiedFrom.cpId;
            if (verData.copyFlag) {
                //When they are copying from TC to TC , TC to FN o, FN to FN , copy as new version in FN and TC
                if (from.copiedFrom && from.copiedFrom.cpType == "TC") {
                    tdQuery.testcaseId = from.copiedFrom.cpId;
                    //For Copying from testcase to function below two lines is added
                    if (tdQuery.type == "FN") tdQuery.type = "TC";
                    if (tdQuery.functionId) delete tdQuery.functionId;
                } else if (from.copiedFrom && from.copiedFrom.cpType == "FN") {
                    //For Copying from function to function below two lines is added
                    if (tdQuery.testcaseId) delete tdQuery.testcaseId;
                    tdQuery.functionId = from.copiedFrom.cpId;
                }
            }
            dbManager.getData("testdata_environments", tdQuery, async function (err, dataPresent) {
                if (err) {
                    // cb(err);
                    reject(err)
                } else {
                    data.moduleId = from.moduleId;
                    let tdNew;
                    let tdData = {};
                    if (dataPresent.length) {
                        tdNew = false;
                        if (dataPresent && dataPresent.length && dataPresent[0].moduleId != from.moduleId) {
                            // update all the testdata and testdata_environments with new moduleId
                            let query = { customerId: dataPresent[0].customerId, projectId: dataPresent[0].projectId, deleted: false };
                            if (dataPresent[0].functionId) {
                                query.functionId = dataPresent[0].functionId;
                            } else if (dataPresent[0].testcaseId) {
                                query.testcaseId = dataPresent[0].testcaseId;
                            }
                            if (query.functionId || query.testcaseId) {
                                await dbManager.asyncUpdate("testdata", query, { $set: { moduleId: from.moduleId } })
                                await dbManager.asyncUpdate("testdata_environments", query, { $set: { moduleId: from.moduleId } })
                            }

                        }
                        if (dataPresent.length == 1 && (data.version == 1)) {
                            tdData.presentVerData = dataPresent[0];
                        } else if (dataPresent.length == 2) {
                            if (dataPresent[0].version == step.version) {
                                tdData.presentVerData = dataPresent[0];
                                tdData.previousVerData = dataPresent[1];
                            } else {
                                tdData.presentVerData = dataPresent[1];
                                tdData.previousVerData = dataPresent[0];
                            }
                        } else {
                            tdData.previousVerData = dataPresent[0];
                        }
                        data.createdBy = from.createdBy;
                        if (from.updatedBy) data.updatedBy = from.updatedBy;
                        // data.moduleId = from.moduleId;
                        let tdversion;
                        if (!(data["version"] instanceof Object)) {
                            tdversion = 1;
                        }
                        else {
                            tdversion = data.version["$in"][0];
                        }
                        let [err2, data2] = await to(dbManager.restructureData(functionMap, tdData, tdNew, data, newIteration, versionName, tdversion, environmentType, parentId, verData))
                        if (err2) {
                            reject(err2)
                            return;
                        }
                        resolve(data2)
                    } else {
                        tdNew = true;
                        data.createdBy = from.createdBy;
                        if (from.updatedBy) data.updatedBy = from.updatedBy;
                        data.moduleId = from.moduleId;
                        let [err2, data2] = await to(dbManager.restructureData(functionMap, tdData, tdNew, data, newIteration, versionName, 1, environmentType, parentId, verData))
                        if (err2) {
                            reject(err2)
                            return;
                        }
                        resolve(data2)
                    }
                }
            });
        } catch (error) {
            reject(error)
        }
    })
}

DBManager.prototype.restructureData = function (functionMap, tdData, tdNew, data, newIteration, versionName, version, environmentType, parentId, verData) {
    return new Promise(async (resolve, reject) => {
        try {
            if (tdNew) {
                let err1, data1;
                data.version = 1;
                data.versionName = versionName;
                random = Math.random().toString(26).substr(5, 6);
                data.defaultIteration = random;
                data.iterations = [{}];
                data.iterations[0].itrId = random;
                // if(!verData.tdCopy && verData.copiedFromId){
                //     // if they say no while copying testdata then emptying defalut iteration values
                //     for(let i of newIteration){
                //         if(i.value){
                //             i.value = "";
                //         }
                //     }
                // }
                data.iterations[0].data = newIteration;
                data.environmentType = environmentType;
                if (environmentType == 'general') {
                    let tempData = JSON.parse(JSON.stringify(data))
                    delete tempData.iterations;
                    delete tempData.environmentType;
                    delete tempData.defaultIteration;
                    [err1, data1] = await to(dbManager.saveObjectAsync('testdata', JSON.parse(JSON.stringify(tempData))))
                    if (err1) {
                        reject(err1)
                        return;
                    }
                    parentId = data1.id;
                }
                data.parentId = parentId;
                [err1, data1] = await to(dbManager.saveObjectAsync("testdata_environments", JSON.parse(JSON.stringify(data))))
                if (err1) {
                    reject(err1)
                    return;
                }
                resolve(parentId);
            } else {
                let iterations = [];
                let addToTd = {};
                let err1, data1;
                if (tdData.presentVerData) {
                    iterations = await dbManager.comapreDatas(tdData.presentVerData.iterations, newIteration, false, verData)
                    if (verData.copyFlag) {
                        data.defaultIteration = iterations[0].itrId;
                        data.version = version;
                        data.versionName = versionName;
                        data.iterations = iterations;
                        addToTd = data;
                    } else {
                        addToTd = {
                            id: tdData.presentVerData.id,
                            updateFields: {
                                updatedOn: new Date(),
                                iterations: iterations,
                                moduleId: data.moduleId,
                                deleted: data.deleted
                            }
                        }
                    }
                } else {
                    iterations = await dbManager.comapreDatas(tdData.previousVerData.iterations, newIteration, true, verData)
                    data.defaultIteration = iterations[0].itrId;
                    data.version = version;
                    data.versionName = versionName;
                    if (verData.tdCopy) data.iterations = iterations;
                    else data.iterations = [iterations[0]];
                    addToTd = data;
                }
                if (environmentType == 'general') {
                    let tempData = JSON.parse(JSON.stringify(addToTd))
                    if (addToTd.updateFields) {
                        delete tempData.updateFields.iterations
                    } else {
                        delete tempData.iterations;
                        delete tempData.defaultIteration;
                        delete tempData.environmentType;
                    }
                    [err1, data1] = await to(dbManager.saveObjectAsync("testdata", JSON.parse(JSON.stringify(tempData))))
                    if (err1) {
                        reject(err1)
                        return;
                    }
                    if (!addToTd.updateFields) {
                        parentId = JSON.parse(JSON.stringify(data1.id))
                    }
                }
                if (!addToTd.updateFields) {
                    addToTd.parentId = JSON.parse(JSON.stringify(parentId));
                }
                addToTd.environmentType = environmentType;
                [err1, data1] = await to(dbManager.saveObjectAsync("testdata_environments", JSON.parse(JSON.stringify(addToTd))))
                if (err1) {
                    reject(err1)
                    return;
                }
                resolve(parentId);
            }
        } catch (error) {
            reject(error)
            console.log(error)
        }
    })
}

DBManager.prototype.comapreDatas = function (previousVerData, postData, newItr, verData) {
    return new Promise((resolve) => {
        let preStepIds = previousVerData[0].data.map(e => { return e.stepId });
        let postStepIds = postData.map(e => { return e.stepId });
        let newIteration = [];
        if (!verData.copyFlag) {
            if (!newItr) {
                for (let j = 0; j < previousVerData.length; j++) {
                    newIteration.push({ itrId: previousVerData[j].itrId, name: previousVerData[j].name, data: [] });
                }
            } else {
                for (let j = 0; j < previousVerData.length; j++) {
                    newIteration.push({ itrId: Math.random().toString(26).substr(5, 6), name: "", data: [] });
                }
            }

            for (let i = 0; i < postStepIds.length; i++) {
                let index = preStepIds.indexOf(postStepIds[i]);
                let j = 0, newvar = 0;
                while ((index = preStepIds.indexOf(postStepIds[i], j++)) != -1 && newvar != 1) {
                    if (previousVerData[0].data[index].type == "OB") {
                        if (previousVerData[0].data[index].name == postData[i].name && previousVerData[0].data[index].paramtype == postData[i].paramtype) {
                            newvar = 1;
                            for (let k = 0; k < previousVerData.length; k++) {
                                newIteration[k].data.push(previousVerData[k].data[index])
                            }
                        }
                    } else {
                        if ((previousVerData[0].data[index].functionId == postData[i].functionId) && (previousVerData[0].data[index].version == postData[i].version)) {
                            newvar = 1;
                            for (let k = 0; k < previousVerData.length; k++) {
                                previousVerData[k].data[index].name = postData[i].name;
                                newIteration[k].data.push(previousVerData[k].data[index])
                            }
                        }
                    }
                }
                if (newvar == 0) {
                    for (let k = 0; k < previousVerData.length; k++) {
                        newIteration[k].data.push(postData[i])
                    }
                }
            }
        } else {
            if (verData.tdCopy) {
                for (let i of previousVerData) {
                    let paramMap = {};
                    let functionMap = {}
                    for (let j of i.data) {
                        if (j.type == "OB") {
                            if (j.paramId)
                                paramMap[j.paramId] = j;
                        }
                        if (j.type == "FN") {
                            if (j.functionId)
                                functionMap[j.functionId] = j;
                        }
                    }
                    let postTD = JSON.parse(JSON.stringify(postData));
                    for (let j of postTD) {
                        if (j.type == "OB") {
                            if (j.paramId && paramMap[j.paramId]) {
                                j.value = paramMap[j.paramId].value;
                            }
                        }
                        if (j.type == "FN") {
                            if (j.functionId && functionMap[j.functionId]) {
                                j.iterationSelected = functionMap[j.functionId].iterationSelected;
                            }
                        }
                    }
                    newIteration.push({ itrId: Math.random().toString(26).substr(5, 6), name: i.name, data: postTD })
                }
            } else {
                newIteration.push({ itrId: Math.random().toString(26).substr(5, 6), name: "", data: postData })
            }
        }
        resolve(newIteration);
    })

}

async function deleteDefalutValForLocalParams(objectValue, steps) {
    try {
        let paramNames = [];
        for (let step of steps) {
            if (step.parameters) {
                for (let i = 0; i < step.parameters.length; i++) {
                    paramNames.push(new RegExp("^" + step.parameters[i].name + "$", "i"))
                }
            }
        }
        if (paramNames.length) {
            let parameters = await dbManager.getasynQuery("parameter", { "query": { "customerId": objectValue.customerId, "projectId": objectValue.projectId, "deleted": false, "name": { $in: paramNames } } })
            let paramIds = [];
            if (parameters.length) {
                for (let param of parameters) {
                    if (param.type == "LOCAL" && param.defaultValue) {
                        paramIds.push(param.id);
                    }
                }
            }
            if (paramIds.length)
                await dbManager.asyncUpdate("parameter", { id: { $in: paramIds }, deleted: false, "customerId": objectValue.customerId, "projectId": objectValue.projectId }, { $set: { defaultValue: null } })
        }
    } catch (error) {
        console.log(error);
    }
}


module.exports = DBManager;
